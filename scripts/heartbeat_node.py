#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
import Jetson.GPIO as GPIO


class HeartbeatNode():
    def __init__(self):
        self.led_pin = 7
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.led_pin, GPIO.OUT, initial=GPIO.HIGH)
        self.pub = rospy.Publisher("/hearbeat", String, queue_size=10)

        self.topic_to_listen = "/camera/infra1/image_rect_raw"
        self.msg_to_listen = Image
        #self.topic_to_listen = "/test"
        #self.msg_to_listen = String
        self.sub = rospy.Subscriber(self.topic_to_listen, self.msg_to_listen, self.cbMsg)
        self.led_status = False

    def cbMsg(self, msg):
        if hasattr(msg, 'header'):
            if msg.header.seq % 10 != 0:
               return 
        new_value = GPIO.HIGH if self.led_status else GPIO.LOW
        GPIO.output(self.led_pin, new_value)
        self.led_status = not self.led_status
        self.pub.publish(String(str(new_value)))


if __name__ == "__main__":
    rospy.init_node("heartbeat_node")
    n = HeartbeatNode()
    rospy.spin()
